# tslint-config-bic
Включает в себя сборку правила из [tslint-config-aribnb](https://github.com/progre/tslint-config-airbnb) с некоторыми изменениями.

## Usage
1. Install 
````sh
yarn add --dev http://192.168.1.70:8888/Infrastructure/tslint-config-bic.git#^${VERSION}
````

2. Add `"extends": "tslint-config-bic"` to your tslint.json.
