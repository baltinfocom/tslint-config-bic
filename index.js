module.exports = {
	extends: [
		'tslint:recommended',
		'tslint-config-standard',
		'tslint-consistent-codestyle',
		'tslint-microsoft-contrib/latest',
		'./tslint.yaml',
	],
};
